(require("dotenv")).config()
const express = require("express")
const cors = require("cors")
const restRouters = require("./apiRest/routers")
const grapqlApi = require("./apiGraphQL/expressMiddleware")
const app = express()

const port = process.env.PORT || 8000

app.listen(port, () => console.log("Listening on port "+port))

app.use(cors())

//REST API
app.use(restRouters)

//GRAPHQL API
app.use(grapqlApi)