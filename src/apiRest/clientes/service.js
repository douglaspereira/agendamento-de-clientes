const mongo = require("../../database/mongoClient")
const serviceResponse =  require("../commons/serviceResponse")

async function inserirCliente(collection){
    let client, result;

    try {
        client = await mongo.connect()
        const clienteCollection = client.db().collection("clientes")

        result = (await clienteCollection.insertOne(collection)).ops[0]
        // console.log("INSERT OK: ", result)
    } catch (err){
        console.error(err.message)
        result = err.message;
    } finally {
        if (client) client.close()
    }
    return result;
}

async function buscarClientes(_id, nome){
    let client, result;

    try{
        let client = await mongo.connect()
        const clienteCollection = client.db().collection("clientes")

        let query = {}
        if (_id) query._id = mongo.ObjectId(_id)
        if (nome) query.nome = {$regex : `.*${nome}.*`, $options: 'i'} 
        
        result = await clienteCollection.find(query).toArray()
    } catch (err){
        console.error(err.message)
        result = err.message;
    } finally {
        if (client) client.close()
    }
    return result;
}

async function atualizarCliente(_id, update){
    let client, result;

    try{
        console.log(update)
        let client = await mongo.connect()
        const clienteCollection = client.db().collection("clientes")

        result = await clienteCollection.updateOne({_id: mongo.ObjectId(_id)}, {$set: update}, { writeConcern: true })
        // console.log(`UPDATE ${_id}:`, result.result.n !== 0)
        result = new serviceResponse(result.result.n !== 0 ? 200 : 404, "OK", undefined)
    } catch (err){
        console.error(err.message)
        result = new serviceResponse(400, null, err.message)
    } finally {
        if (client) client.close()
    }
    return result;
}

async function deletarCliente(_id, nome){
    let client, result;

    try{
        let client = await mongo.connect()
        const clienteCollection = client.db().collection("clientes")
        
        result = await clienteCollection.deleteOne({_id: mongo.ObjectId(_id)})
        // console.log(`DELETE ${_id}:`, result.result.n != 0)

        result = result.result.n !== 0 
            ? new serviceResponse(200, "OK", undefined) 
            : new serviceResponse(404, null, undefined)

    } catch (err){
        console.log(err.message)
        result = new serviceResponse(400, null, err.message)
    } finally {
        if (client) client.close()
    }
    
    return result;
}


module.exports = {inserirCliente, buscarClientes, atualizarCliente, deletarCliente}