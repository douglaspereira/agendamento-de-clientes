const express = require("express")
const router = express.Router()
const service = require("./service")

router.get("/:id?", async function (req, res) {   
    res.send(await service.buscarClientes(req.params.id, req.query.nome))
    })

router.post("/", async function(req, res){
    res.send(await service.inserirCliente(req.body))
    })

router.put("/:id", async function(req, res){
    const response = await service.atualizarCliente(req.params.id, req.body);
    return res.status(response.status || 500).send(response.data || response.err)
    })

router.delete("/:id?", async function(req, res){
    const response = await service.deletarCliente(req.params.id, req.params.nome)
    return res.status(response.status || 500).send(response.data || response.err)
    })

module.exports = router