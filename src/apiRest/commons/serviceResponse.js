class serviceResponse {
    constructor(status = 200, data = {}, err = ""){
    this.status = status
    this.data = data
    this.err = err
    }
}

module.exports = serviceResponse;