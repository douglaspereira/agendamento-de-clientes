const clienteController = require("./clientes/controller")
const express = require("express")
const bodyParser = require("body-parser")
const router = express.Router();

router.use(bodyParser.json())

router.use("/clientes", clienteController);


module.exports = router