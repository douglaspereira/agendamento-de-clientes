const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const mongoConfig = require('./mongoConfig');

async function connect(){
// Create a new MongoClient
const client = new MongoClient(mongoConfig.url, { useUnifiedTopology: true } );

// Use connect method to connect to the Server
result = await client.connect();
//console.log("Connected successfully to server");
client._db = client.db
client.db = () => client._db(mongoConfig.dbName)
return client;
}

function ObjectId(id){
    return new mongo.ObjectID(id)
}

module.exports = {connect, ObjectId}