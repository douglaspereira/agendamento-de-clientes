// Connection URL
const url = process.env.MONGO_URL || 'mongodb://localhost:27017';

// Database Name
const dbName = process.env.MONGO_DBNAME || 'agendamentos';

module.exports = {url, dbName}