const { GraphQLDateTime } = require("graphql-iso-date");

const  customScalarsResolvers = {DateTime: GraphQLDateTime}

module.exports = customScalarsResolvers