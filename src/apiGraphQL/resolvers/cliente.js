const provider = require("../providers/cliente")

const resolvers = {
        clientes: ({_id}) => provider.find(_id),
        createCliente: ({_id, cliente}) => provider.insert(_id, cliente),
        updateCliente: ({_id, cliente}) => provider.update(_id, cliente),
        deleteCliente: ({_id}) => provider.remove(_id)
};

module.exports = resolvers;