const provider = require("../providers/agendamento")

const resolvers = {
        agendamentos: ({_id}) => provider.find(_id),
        createAgendamento: ({_id, agendamento}) => provider.insert(_id, agendamento),
        updateAgendamento: ({_id, agendamento}) => provider.update(_id, agendamento),
        deleteAgendamento: ({_id}) => provider.remove(_id)
};

module.exports = resolvers;