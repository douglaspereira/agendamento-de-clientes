const cliente = require("./cliente")
const agendamento = require("./agendamento")

const resolvers = {...cliente, ...agendamento}

module.exports = resolvers