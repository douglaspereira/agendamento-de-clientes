const mongo = require("../../database/mongoClient")


async function find(_id){
    let client = await mongo.connect()
    const agendaCollection = client.db().collection("agendamentos")

    let query =  [
        {
            $lookup: {
              from: 'clientes',
              localField: 'idCliente',
              foreignField: '_id',
              as: 'cliente'
            }
          },
          { $unwind : "$cliente" }
    ]
    if (_id) query.push({ $match : {_id: mongo.ObjectId(_id)}})

    let response = await agendaCollection.aggregate(query).toArray()
    
    client.close()
    return response
}

async function insert(_id, agendamento){
    let client = await mongo.connect()
    const clienteCollection = client.db().collection("clientes")
    let cliente = await clienteCollection.find({_id: mongo.ObjectId(agendamento.idCliente)}).toArray()
    if (cliente.length == 0) throw new Error("Cliente não existe")

    agendamento.idCliente = mongo.ObjectId(agendamento.idCliente)

    const agendaCollection = client.db().collection("agendamentos")
    if (_id) agendamento._id = mongo.ObjectId(_id)
    let response = (await agendaCollection.insertOne(agendamento)).ops[0]
    client.close()    
    return response;
}

async function update(_id, agendamento) {
    let client = await mongo.connect()
    const agendaCollection = client.db().collection("agendamentos")

    agendamento.idCliente = mongo.ObjectId(agendamento.idCliente)
            
    let response = await agendaCollection.updateOne({_id: mongo.ObjectId(_id)}, {$set: agendamento}, { writeConcern: true })

    response = response.result.nModified !== 0
    client.close()
    
    return response;

}

async function remove(_id) {
    let client, response;

    client = await mongo.connect()
    const agendaCollection = client.db().collection("agendamentos")
    
    response = await agendaCollection.deleteOne({_id: mongo.ObjectId(_id)})
    response = response.result.n !== 0 
    client.close()
    return response
}

module.exports = {find, insert, update, remove}