const mongo = require("../../database/mongoClient")


async function find(_id){
    let client = await mongo.connect()
    const clienteCollection = client.db().collection("clientes")
    let query = {}
    if (_id) query._id = mongo.ObjectId(_id)
    let response = await clienteCollection.find(query).toArray()
    client.close()
    return response
}

async function insert(_id, cliente){
    if (!cliente.nome) throw new Error("nome cannot be null")
    if (_id) cliente._id = mongo.ObjectId(_id)

    let client = await mongo.connect()
    const clienteCollection = client.db().collection("clientes")
    let response = (await clienteCollection.insertOne(cliente)).ops[0]
    client.close()    
    return response;
}

async function update(_id, cliente) {
    let client = await mongo.connect()
    const clienteCollection = client.db().collection("clientes")

    let response = await clienteCollection.updateOne({_id: mongo.ObjectId(_id)}, {$set: cliente}, { writeConcern: true })
    response = response.result.n !== 0
    client.close()
    
    return response;

}

async function remove(_id) {
    let client, response;

    client = await mongo.connect()
    const clienteCollection = client.db().collection("clientes")
    
    response = await clienteCollection.deleteOne({_id: mongo.ObjectId(_id)})
    response = response.result.n !== 0 
    client.close()
    return response
}


module.exports = {find, insert, update, remove}