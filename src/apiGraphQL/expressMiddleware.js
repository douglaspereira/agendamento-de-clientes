const router = (require("express")).Router();
const graphqlHTTP = require("express-graphql");
const { makeExecutableSchema } = require("graphql-tools");
const resolverRoot = require("./resolvers/root")
const customScalars = require("./resolvers/customScalars")
const {importSchema} =  require("graphql-import")
const typeDefsCliente = importSchema("./src/apiGraphQL/schemas/root.gql")

const schema = makeExecutableSchema({typeDefs: typeDefsCliente, resolvers: customScalars})

router.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: resolverRoot,
  graphiql: true,
  }));

  module.exports = router