# Agendamento De Atendimentos à Clientes

## Funcionalidades da API
- Cadastrar Clientes: [nome, telefone]
- Alterar Clientes
- Cadastrar de um Agenda: [dataInicio, dataFim, Cliente]
- *Ver Agendamentos pendentes e já atendidos
- *Poder desmarcar um Agendamento
- *Receber o aviso quando estiver próximo a Agenda
- *Ver quantos Agendamentos já foram feitos com um ou vários Clientes


## Tecnologias/Conceitos utilizadas(os):
- REST: Utilizado inicialmente para a criação de webservices, feito  endpoints de /clientes 
- GRAPHQL: Arquitetura para a comunicação com a API
- Gitlab: Utilizado para o repositório e Board com tarefas a serem implementadas
- Postman: Automação de testes da API (utilizado no endpoint /clientes)
- *RabitMQ: Avisar o usuário de horário próximo ao agendamento
- *Jest: para testes unitários 
- CleanCode: Visando sempre um código organizado, modularizado e autoexplicativo

 \* ainda não implementados